package com.mathieu.lopez.numberslight.presenter

import com.mathieu.lopez.numberslight.model.ItemModel
import com.mathieu.lopez.numberslight.presenter.ListPresenter.View
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ListPresenterTest {

    @Mock
    private lateinit var view: View

    private lateinit var listPresenter: ListPresenter

    @Before
    fun setup() {
        listPresenter = ListPresenter(view)
    }

    @Test
    fun convertModelToData_should_return_items_list_when_items_models_list_is_valid() {
        val itemsModelsList: ArrayList<ItemModel> = ArrayList()
        for (i in 0..10) {
            itemsModelsList.add(ItemModel("Test $i", "img"))
        }
        val convertModelToData = listPresenter.convertModelToData(itemsModelsList)

        assertEquals(convertModelToData.size, itemsModelsList.size)
        assertEquals(convertModelToData[5].name, "Test 5")
    }

    @Test
    fun convertModelToData_should_return_empty_list_when_model_is_null() {
        val convertModelToData = listPresenter.convertModelToData(null)
        assertNotNull(convertModelToData)
    }
}