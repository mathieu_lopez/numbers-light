package com.mathieu.lopez.numberslight.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.mathieu.lopez.numberslight.R
import com.mathieu.lopez.numberslight.presenter.ListPresenter
import com.mathieu.lopez.numberslight.presenter.data.Item
import com.mathieu.lopez.numberslight.view.adapter.ItemListAdapter
import kotlinx.android.synthetic.main.custom_recycler.*
import kotlinx.android.synthetic.main.item_list.*

class ListActivity : AppCompatActivity(), ListPresenter.View {

    private var twoPane: Boolean = false
    private var presenter: ListPresenter = ListPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        if (itemDetailContainer != null) {
            twoPane = true
        }

        presenter.loadInformation()

        customRecyclerErrorViewButton.setOnClickListener {
            presenter.loadInformation()
        }
    }

    override fun onStartLoading() {
        customRecyclerListView.visibility = GONE
        customRecyclerErrorView.visibility = GONE
        customRecyclerProgressView.visibility = VISIBLE
    }

    override fun onLoadingSuccess(items: List<Item>) {
        customRecyclerListView.visibility = VISIBLE
        customRecyclerErrorView.visibility = GONE
        customRecyclerProgressView.visibility = GONE
        customRecyclerListView.adapter = ItemListAdapter(this, items, twoPane)
    }

    override fun onLoadingFail(message: String) {
        customRecyclerListView.visibility = GONE
        customRecyclerErrorView.visibility = VISIBLE
        customRecyclerProgressView.visibility = GONE
        Toast.makeText(this, R.string.error_toast_message, Toast.LENGTH_LONG).show()
    }
}
