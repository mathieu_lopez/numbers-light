package com.mathieu.lopez.numberslight.presenter.data

data class Item(val name: String, val image: String)