package com.mathieu.lopez.numberslight.view

import android.R.id.home
import android.content.Intent
import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.mathieu.lopez.numberslight.R
import com.mathieu.lopez.numberslight.view.DetailFragment.Companion.ARG_ITEM_ID

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (resources.configuration.orientation == ORIENTATION_LANDSCAPE && resources.getBoolean(R.bool.isTablet)) {
            finish()
        }

        if (savedInstanceState == null) {
            val fragment = DetailFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_ITEM_ID, intent.getStringExtra(ARG_ITEM_ID))
                }
            }
            supportFragmentManager.beginTransaction().add(R.id.itemDetailContainer, fragment).commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            home -> {
                NavUtils.navigateUpTo(this, Intent(this, ListActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}
