package com.mathieu.lopez.numberslight.model

data class ItemDetailModel(val name: String, val text: String, val image: String)