package com.mathieu.lopez.numberslight.view.adapter

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.mathieu.lopez.numberslight.R
import com.mathieu.lopez.numberslight.presenter.data.Item
import com.mathieu.lopez.numberslight.view.DetailFragment
import com.mathieu.lopez.numberslight.view.DetailFragment.Companion.ARG_ITEM_ID
import com.mathieu.lopez.numberslight.view.DetailJavaActivity
import com.mathieu.lopez.numberslight.view.ListActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item.view.*

class ItemListAdapter(
    private val parentActivity: ListActivity,
    private val values: List<Item>,
    private val twoPane: Boolean
) :
    RecyclerView.Adapter<ItemListAdapter.ViewHolder>() {

    private val onClickListener: View.OnClickListener
    private val onFocusListener: View.OnFocusChangeListener

    init {
        onFocusListener = View.OnFocusChangeListener { view, _ -> onClickItem(view) }
        onClickListener = View.OnClickListener { view -> onClickItem(view) }
    }

    private fun onClickItem(view: View) {
        val item = view.tag as Item
        if (twoPane) { // When two pan is visible, we set a detail fragment beside a list
            val fragment = DetailFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_ITEM_ID, item.name)
                }
            }
            parentActivity.supportFragmentManager
                .beginTransaction()
                .replace(R.id.itemDetailContainer, fragment)
                .commit()
        } else { // Otherwise we start a detail activity
            val intent = Intent(view.context, DetailJavaActivity::class.java).apply {
                putExtra(ARG_ITEM_ID, item.name)
            }
            view.context.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = values.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]

        holder.itemName.text = item.name
        Picasso.get().load(item.image).into(holder.itemImage)

        with(holder.itemView) {
            tag = item
            if (twoPane) { // when two pan is visible we use onFocusChangeListener for item focus color
                holder.itemView.isFocusableInTouchMode = true
                onFocusChangeListener = onFocusListener
            } else { // Otherwise we use simple onClickListener
                holder.itemView.isFocusableInTouchMode = false
                setOnClickListener(onClickListener)
            }
        }
    }

    // Item view holder
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val itemName: TextView = view.itemName
        val itemImage: ImageView = view.itemImage
    }
}