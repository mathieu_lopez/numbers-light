package com.mathieu.lopez.numberslight.presenter

import com.mathieu.lopez.numberslight.ApiService
import com.mathieu.lopez.numberslight.model.ItemDetailModel
import com.mathieu.lopez.numberslight.presenter.data.ItemDetail
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailPresenter(private val view: View) {

    private val apiService by lazy {
        ApiService.create()
    }

    fun loadInformation(name: String) {
        view.onStartLoading()
        apiService.getItemDetail(name).enqueue(object : Callback<ItemDetailModel> {
            override fun onFailure(call: Call<ItemDetailModel>, t: Throwable) {
                view.onLoadingFail("Can't be connect to server, network error or the server not respond")
            }

            override fun onResponse(call: Call<ItemDetailModel>, response: Response<ItemDetailModel>) {
                val responseDetail = response.body()
                if (response.isSuccessful && responseDetail != null) {
                    view.onLoadingSuccess(
                        ItemDetail(
                            responseDetail.name,
                            responseDetail.text,
                            responseDetail.image
                        )
                    )
                } else {
                    val errorBody = response.errorBody()
                    if (errorBody != null) {
                        view.onLoadingFail(errorBody.string())
                    } else {
                        // When can't be get error body we generate and other error
                        view.onLoadingFail("Receive an error")
                    }
                }
            }
        })
    }

    interface View {
        fun onStartLoading()
        fun onLoadingSuccess(itemDetail: ItemDetail)
        fun onLoadingFail(message: String)
    }
}