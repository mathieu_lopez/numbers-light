package com.mathieu.lopez.numberslight.model

data class ItemModel(val name: String, val image: String)