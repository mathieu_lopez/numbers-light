package com.mathieu.lopez.numberslight

import com.mathieu.lopez.numberslight.Constant.BASE_URL
import com.mathieu.lopez.numberslight.model.ItemDetailModel
import com.mathieu.lopez.numberslight.model.ItemModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiService {

    @GET("json.php")
    fun getItemsList(): Call<Collection<ItemModel>>

    @GET("json.php")
    fun getItemDetail(@Query("name") number: String): Call<ItemDetailModel>

    companion object {
        fun create(): ApiService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .client(generateClient())
                .build()

            return retrofit.create(ApiService::class.java)
        }

        private fun generateClient(): OkHttpClient {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BASIC
            val httpClient = OkHttpClient.Builder()
            if (BuildConfig.DEBUG) {
                httpClient.addInterceptor(logging)
            }
            return httpClient.build()
        }
    }
}