package com.mathieu.lopez.numberslight.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import com.mathieu.lopez.numberslight.R;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;
import static com.mathieu.lopez.numberslight.view.DetailFragment.ARG_ITEM_ID;

public class DetailJavaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        // When device is a tablet and is landscape orientation we finish activity to return list activity
        if (getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE && getResources().getBoolean(R.bool.isTablet)) {
            finish();
        }

        if (savedInstanceState == null) {
            DetailFragment detailFragment = new DetailFragment();
            Bundle bundle = new Bundle();
            bundle.putString(ARG_ITEM_ID, getIntent().getStringExtra(ARG_ITEM_ID));
            detailFragment.setArguments(bundle);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.itemDetailContainer, detailFragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(this, ListActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
