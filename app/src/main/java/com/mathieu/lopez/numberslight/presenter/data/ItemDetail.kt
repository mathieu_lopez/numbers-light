package com.mathieu.lopez.numberslight.presenter.data

data class ItemDetail(val name: String, val text: String, val image: String)