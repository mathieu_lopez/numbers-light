package com.mathieu.lopez.numberslight.presenter

import com.mathieu.lopez.numberslight.ApiService
import com.mathieu.lopez.numberslight.model.ItemModel
import com.mathieu.lopez.numberslight.presenter.data.Item
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListPresenter(private val view: View) {

    private val apiService by lazy {
        ApiService.create()
    }

    fun loadInformation() {
        view.onStartLoading()
        apiService.getItemsList().enqueue(object : Callback<Collection<ItemModel>> {
            override fun onFailure(call: Call<Collection<ItemModel>>, t: Throwable) {
                view.onLoadingFail("Can't be connect to server, network error or the server not respond")
            }

            override fun onResponse(call: Call<Collection<ItemModel>>, response: Response<Collection<ItemModel>>) {
                val responseItems = response.body()
                if (response.isSuccessful && responseItems != null) {
                    view.onLoadingSuccess(convertModelToData(responseItems))
                } else {
                    val errorBody = response.errorBody()
                    if (errorBody != null) {
                        view.onLoadingFail(errorBody.string())
                    } else {
                        // When can't be get error body we generate and other error
                        view.onLoadingFail("Receive an error")
                    }
                }
            }
        })
    }

    fun convertModelToData(itemsModelsList: Collection<ItemModel>?): ArrayList<Item> {
        val itemsList: ArrayList<Item> = ArrayList()
        itemsModelsList?.forEach {
            itemsList.add(Item(it.name, it.image))
        }
        return itemsList
    }

    interface View {
        fun onStartLoading()
        fun onLoadingSuccess(items: List<Item>)
        fun onLoadingFail(message: String)
    }
}