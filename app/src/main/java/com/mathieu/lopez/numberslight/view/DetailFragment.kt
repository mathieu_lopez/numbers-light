package com.mathieu.lopez.numberslight.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.*
import com.mathieu.lopez.numberslight.R
import com.mathieu.lopez.numberslight.presenter.DetailPresenter
import com.mathieu.lopez.numberslight.presenter.data.ItemDetail
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_detail.view.*

class DetailFragment : Fragment(), DetailPresenter.View {

    private val detailPresenter: DetailPresenter = DetailPresenter(this)

    private var detailText: TextView? = null
    private var detailImage: ImageView? = null
    private var detailView: LinearLayout? = null
    private var detailErrorView: LinearLayout? = null
    private var detailProgressView: FrameLayout? = null
    private var name: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_ITEM_ID)) {
                name = it.getString(ARG_ITEM_ID)
                detailPresenter.loadInformation(name!!)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.item_detail, container, false)

        detailText = rootView.itemDetailText
        detailImage = rootView.itemDetailImage
        detailView = rootView.itemDetailView
        detailErrorView = rootView.itemDetailErrorView
        detailProgressView = rootView.itemDetailProgressView

        rootView.itemDetailErrorViewButton.setOnClickListener {
            if (name != null) {
                detailPresenter.loadInformation(name!!)
            }
        }

        return rootView
    }

    override fun onStartLoading() {
        if (detailView != null) {
            detailView!!.visibility = GONE
        }
        if (detailErrorView != null) {
            detailErrorView!!.visibility = GONE
        }
        if (detailProgressView != null) {
            detailProgressView!!.visibility = VISIBLE
        }
    }

    override fun onLoadingSuccess(itemDetail: ItemDetail) {
        if (detailView != null) {
            detailView!!.visibility = VISIBLE
        }
        if (detailErrorView != null) {
            detailErrorView!!.visibility = GONE
        }
        if (detailProgressView != null) {
            detailProgressView!!.visibility = GONE
        }
        if (detailText != null) {
            detailText!!.text = itemDetail.text
        }
        if (detailImage != null) {
            Picasso.get().load(itemDetail.image).into(detailImage)
        }
    }

    override fun onLoadingFail(message: String) {
        if (detailView != null) {
            detailView!!.visibility = GONE
        }
        if (detailErrorView != null) {
            detailErrorView!!.visibility = VISIBLE
        }
        if (detailProgressView != null) {
            detailProgressView!!.visibility = GONE
        }
        Toast.makeText(activity, R.string.error_toast_message, Toast.LENGTH_LONG).show()
    }

    companion object {
        const val ARG_ITEM_ID = "item_id"
    }
}